package com.tech.facescan;

public class ConstantValues {

    public static String minTime = "minTime";
    public static String maxTime = "maxTime";
    public static String UITextColor = "UITextColor";
    public static String UIEnable = "UIEnable";
    public static String UIDrawColor = "UIDrawColor";
    public static String UIDrawType = "UIDrawType";
    public static String BOX = "BOX";
    public static String CORNER = "CORNER";
}

package com.tech.facescan;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import org.json.JSONArray;
import org.json.JSONObject;

public class StartFaceScan extends AppCompatActivity implements CallBackListeners {

    FragmentContainerView fragmentContainerView;
    FaceScan faceScan;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.face_scan);
        fragmentContainerView = findViewById(R.id.fragment_container);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        try {
            JSONObject param = new JSONObject();
            param.put(ConstantValues.minTime,30);
            param.put(ConstantValues.maxTime,60);
            param.put(ConstantValues.UITextColor,"#6bb8f8");
            param.put(ConstantValues.UIDrawColor,"#FFFFFF");
            param.put(ConstantValues.UIDrawType,ConstantValues.CORNER);
            param.put(ConstantValues.UIEnable,true); //
            faceScan = new FaceScan(param);
            ft.replace(R.id.fragment_container, faceScan, "CameraFragment");
            ft.commit();
        }catch (Exception e){
            e.printStackTrace();
        }

        //faceScan.StopScan();
    }


    @Override
    public void OnFrame(String type, int percentage, String fps,boolean isFaceDetected) {
        Log.e("onScanFinishedCallback",""+fps);
    }

    @Override
    public void onScanFinished(JSONArray raw_intensity, JSONArray ppg_time, int average_fps, JSONObject sendData) {
        Log.e("onScanFinishedCallback",""+average_fps);
        Intent intent = new Intent(StartFaceScan.this, Result.class);
        intent.putExtra("jsonData",""+sendData);
        startActivity(intent);
        finish();
    }

    @Override
    public void onError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        //finish();
    }

    @Override
    public void onCancelScan() {
        Toast.makeText(this, "Cancel Scan", Toast.LENGTH_SHORT).show();
        //finish();
    }
}

/*
 * Copyright 2020 Google LLC. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tech.facescan;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.preference.PreferenceManager;

import com.google.mlkit.vision.face.Face;

/**
 * Graphic instance for rendering face position, contour, and landmarks within the associated
 * graphic overlay view.
 */
public class FaceGraphic extends GraphicOverlay.Graphic {
  private static final float BOX_STROKE_WIDTH = 5.0f;
  private final Paint facePositionPaint;
  private final Context context;
  SharedPreferences sharedPreferences;

  private volatile Face face;

  FaceGraphic(GraphicOverlay overlay, Face face, Context context) {
    super(overlay);

    sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    this.face = face;
    this.context = context;
    int paintColor = Color.parseColor(sharedPreferences.getString(ConstantValues.UIDrawColor,"#FFFFFF"));
    facePositionPaint = new Paint();
    facePositionPaint.setColor(paintColor);
    facePositionPaint.setStyle(Paint.Style.STROKE);
    facePositionPaint.setStrokeWidth(BOX_STROKE_WIDTH);
  }

  /** Draws the face annotations for position on the supplied canvas. */
  @Override
  public void draw(Canvas canvas) {
    Face face = this.face;
    if (face == null) {
      return;
    }

    // Draws a circle at the position of the detected face, with the face's track id below.

    if (sharedPreferences.getString(ConstantValues.UIDrawType,ConstantValues.CORNER).equalsIgnoreCase(ConstantValues.BOX) ){
      canvas.drawRect(translateX(face.getBoundingBox().left),translateY(face.getBoundingBox().top),
              translateX(face.getBoundingBox().right),translateY(face.getBoundingBox().bottom),facePositionPaint);
    }else {
      float d =(scale(face.getBoundingBox().width()*0.035f));

      float pts[] = new float[32];
      pts[0]= translateX(face.getBoundingBox().left+d);
      pts[1]= translateY(face.getBoundingBox().top);
      pts[2]= translateX(face.getBoundingBox().left);
      pts[3]= translateY(face.getBoundingBox().top);
      pts[4]= translateX(face.getBoundingBox().left);
      pts[5]= translateY(face.getBoundingBox().top);
      pts[6]= translateX(face.getBoundingBox().left);
      pts[7]= translateY(face.getBoundingBox().top+d);

      pts[8]= translateX(face.getBoundingBox().left);
      pts[9]= translateY(face.getBoundingBox().bottom-d);
      pts[10]= translateX(face.getBoundingBox().left);
      pts[11]= translateY(face.getBoundingBox().bottom);
      pts[12]= translateX(face.getBoundingBox().left);
      pts[13]= translateY(face.getBoundingBox().bottom);
      pts[14]= translateX(face.getBoundingBox().left+d);
      pts[15]= translateY(face.getBoundingBox().bottom);

      pts[16]= translateX(face.getBoundingBox().right-d);
      pts[17]= translateY(face.getBoundingBox().bottom);
      pts[18]= translateX(face.getBoundingBox().right);
      pts[19]= translateY(face.getBoundingBox().bottom);
      pts[20]= translateX(face.getBoundingBox().right);
      pts[21]= translateY(face.getBoundingBox().bottom);
      pts[22]= translateX(face.getBoundingBox().right);
      pts[23]= translateY(face.getBoundingBox().bottom-d);

      pts[24]= translateX(face.getBoundingBox().right);
      pts[25]= translateY(face.getBoundingBox().top+d);
      pts[26]= translateX(face.getBoundingBox().right);
      pts[27]= translateY(face.getBoundingBox().top);
      pts[28]= translateX(face.getBoundingBox().right);
      pts[29]= translateY(face.getBoundingBox().top);
      pts[30]= translateX(face.getBoundingBox().right-d);
      pts[31]= translateY(face.getBoundingBox().top);

      canvas.drawLines(pts, facePositionPaint);
    }
  }

}

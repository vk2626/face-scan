package com.tech.facescan;

import org.json.JSONArray;
import org.json.JSONObject;

public interface CallBackListeners {

    void OnFrame(String type, int percentage, String fps,boolean isFaceDetected);
    void onScanFinished(
            JSONArray raw_intensity,
            JSONArray ppg_time,
            int average_fps,
            JSONObject sendData);
    void onError(String error);
    void onCancelScan();
}

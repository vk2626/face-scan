package com.tech.facescan

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class Result : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.result)
        val jsonData = findViewById<TextView>(R.id.jsonData)
        jsonData.text = intent.getStringExtra("jsonData")
    }
}
package com.tech.facescan;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.face.FaceLandmark;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class FaceScan extends Fragment  implements AdapterView.OnItemSelectedListener, CompoundButton.OnCheckedChangeListener,
        CameraSource.CameraByteCallBack, FaceDetectorProcessor.FaceCallback{

    private static final String FACE_DETECTION = "Face Detection";

    private static final String TAG = "LivePreviewActivity";
    int minTime = 30;
    int maxTime = 60;
    int calibrationCounterTime = 20;

    private CameraSource cameraSource = null;
    private CameraSourcePreview preview;
    private GraphicOverlay graphicOverlay;
    private String selectedModel = FACE_DETECTION;

    CountDownTimer noFaceCounter;
    CountDownTimer closeFaceCounter;
    CountDownTimer countTimer;
    CountDownTimer calibrationCountTimer;
    int startRecording = 0;
    int avgFPSCounter = 0;
    String uiTextColor="#6bb8f8";
    String UIDrawType=ConstantValues.CORNER;
    String UIDrawColor="#FFFFFF";
    boolean isScanCompleted = false;
    boolean isScanCancelled = false;
    boolean uiEnable = true;
    boolean isPaused = false;
    long startTime = 0L;
    long startTime1 = 0L;
    int calibrationTime = 20;
    int counter = 0;
    boolean clicked = true;
    int calibrationFPS = 0;
    int processingCounter = 0;
    JSONObject raw_intensity;
    JSONObject sendData = new JSONObject();
    JSONArray ppg_time = new JSONArray();
    JSONArray rgb = new JSONArray();
    TextView progressText,tv, cancel;
    TextView scanWarning;
    LinearLayout bottomView;

    CallBackListeners callBackListeners;
    Context context;

    SharedPreferences sharedPreferences;
    public FaceScan(){

    }
    public FaceScan(JSONObject param){
        try {
            if (param.has(ConstantValues.minTime))
                minTime = param.getInt(ConstantValues.minTime);

            if (param.has(ConstantValues.maxTime))
                maxTime = param.getInt(ConstantValues.maxTime);

            if (param.has(ConstantValues.UITextColor)) {
                uiTextColor = param.getString(ConstantValues.UITextColor);
            }

            if (param.has(ConstantValues.UIEnable))
                uiEnable = param.getBoolean(ConstantValues.UIEnable);

            if (param.has(ConstantValues.UIDrawType))
                UIDrawType = param.getString(ConstantValues.UIDrawType);

            if (!UIDrawType.equalsIgnoreCase(ConstantValues.BOX) && !UIDrawType.equalsIgnoreCase(ConstantValues.CORNER))
                Log.e("Facescan Initialization","Invalid Draw-Properties Provided.");

            if (param.has(ConstantValues.UIDrawColor))
                UIDrawColor = param.getString(ConstantValues.UIDrawColor);

            if (minTime<60)
                Log.e("Facescan Initialization","Minimum 60 seconds of Scan is Mandatory");

            if (minTime>maxTime)
                Log.e("Facescan Initialization","Total Scan-Time cannot be smaller than Minimum Scan-Time");

            try {
                Color.parseColor(uiTextColor);
                Color.parseColor(UIDrawColor);
            }catch (Exception e){
                Log.e("Facescan Initialization","Invalid Draw-Properties Provided.");
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        callBackListeners = (CallBackListeners)context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_vision_live_preview, container, false);
        getActivity().getWindow().addFlags (WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        context = getContext();
        cancel = root.findViewById(R.id.cancel);
        progressText = root.findViewById(R.id.progressText);
        tv = root.findViewById(R.id.tv);
        scanWarning = root.findViewById(R.id.scanWarning);
        bottomView = root.findViewById(R.id.bottomView);

        tv.setTextColor(Color.parseColor(uiTextColor));
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        sharedPreferences.edit().putString(ConstantValues.UIDrawType,UIDrawType).apply();
        sharedPreferences.edit().putString(ConstantValues.UIDrawColor,UIDrawColor).apply();

        if (!uiEnable){
            bottomView.setVisibility(View.GONE);
        }

        preview = root.findViewById(R.id.preview_view);
        if (preview == null) {
            Log.d(TAG, "Preview is null");
        }
        graphicOverlay = root.findViewById(R.id.graphic_overlay);
        if (graphicOverlay == null) {
            Log.d(TAG, "graphicOverlay is null");
        }


        ToggleButton facingSwitch = root.findViewById(R.id.facing_switch);
        facingSwitch.setOnCheckedChangeListener(this);

        createCameraSource(selectedModel);

        cancel.setOnClickListener(v->{
            isScanCancelled = true;
            callBackListeners.onCancelScan();
            if (getActivity()!=null)
                getActivity().finish();
        });
        return root;
    }

    FaceDetectorProcessor faceDetectorProcessor;
    private void createCameraSource(String model) {
        // If there's no existing cameraSource, create one.
        if (cameraSource == null) {
            cameraSource = new CameraSource((Activity) context, graphicOverlay);
            cameraSource.setFacing(CameraSource.CAMERA_FACING_FRONT);
            cameraSource.setListener(this);
        }
        faceDetectorProcessor = new FaceDetectorProcessor(getContext());
        faceDetectorProcessor.setListener(this);
        cameraSource.setMachineLearningFrameProcessor(faceDetectorProcessor);
    }

    /**
     * Starts or restarts the camera source, if it exists. If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private void startCameraSource() {
        if (cameraSource != null) {
            try {
                if (preview == null) {
                    Log.d(TAG, "resume: Preview is null");
                }
                if (graphicOverlay == null) {
                    Log.d(TAG, "resume: graphOverlay is null");
                }
                preview.start(cameraSource, graphicOverlay);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                cameraSource.release();
                cameraSource = null;
            }
        }
    }

    int scanPercentage;
    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");

        if (clicked){
            if (checkAndRequestPermissions()){
                createCameraSource(selectedModel);
                startCameraSource();

                startTime1 = System.currentTimeMillis();
                calibrationCounterTime = 20;
                calibrationCountTimer = new CountDownTimer(calibrationCounterTime* 1000L, 1000) {
                    @Override
                    public void onTick(long l) {
                        progressText.setText("Calibration in Progress...");
                        calibrationTime--;
                        long endTime = System.currentTimeMillis();
                        long elapsedMilliSeconds = endTime - startTime;
                        int calibrationPercentage = (int) (elapsedMilliSeconds / (calibrationCounterTime*1000.0) * 100);
                        tv.setText(String.format("Scan Starts in %d", calibrationTime));
                        if (callFaceProcess>0 && face!=null) {
                            scanWarning.setVisibility(View.GONE);
                            callBackListeners.OnFrame("calibration",calibrationPercentage,df.format(mFPS),true);
                        }
                        else {
                            scanWarning.setVisibility(View.VISIBLE);
                            scanWarning.setText("Cannot Detect Face");
                            callBackListeners.OnFrame("calibration",calibrationPercentage,df.format(mFPS), false);
                        }
                    }

                    @Override
                    public void onFinish() {
                        startRecording=calibrationCounterTime;
                        startTime = System.currentTimeMillis();
                        countTimer = new CountDownTimer(maxTime* 1000L, 1000) {
                            @Override
                            public void onTick(long l) {
                                long endTime = System.currentTimeMillis();
                                long elapsedMilliSeconds = endTime - startTime;
                                scanPercentage = (int) (elapsedMilliSeconds / (maxTime*1000.0) * 100);
                                progressText.setText( String.format("%d %% Completed", scanPercentage));
                                tv.setText("Scan in progress...");
                                startRecording++;
                                avgFPSCounter++;
                            }

                            @Override
                            public void onFinish() {
                                try {
                                    sendData.put("raw_intensity", rgb);
                                    sendData.put("ppg_time", ppg_time);
                                    sendData.put("average_fps", (processingCounter / avgFPSCounter));
                                    isScanCompleted = true;
                                    callBackListeners.onScanFinished(rgb,ppg_time, (processingCounter / avgFPSCounter),sendData);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }.start();
                    }
                }.start();
            }

        }
    }

    private boolean checkAndRequestPermissions() {
        int cameraPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 123);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 123) {
            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++) {
                    if (permissions[i].equals( Manifest.permission.CAMERA)) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            clicked=false;
                            if(!shouldShowRequestPermissionRationale(permissions[i])){
                                AlertDialog.Builder alertbuilder = new AlertDialog.Builder(context);
                                alertbuilder.setCancelable(false);
                                alertbuilder.setTitle("Camera Permission Required");
                                alertbuilder.setIcon(context.getResources().getDrawable(R.drawable.ic_baseline_camera_alt_24));
                                alertbuilder.setMessage("To continue face scan process you need to allow camera permission. Please go to settings and allow it.");
                                alertbuilder.setPositiveButton("SETTING",
                                        (dialog, which) -> {
                                            clicked = true;
                                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                            intent.setData(Uri.fromParts("package",context.getPackageName(),null));
                                            context.startActivity(intent);
                                        });
                                AlertDialog alertDialog = alertbuilder.create();
                                alertDialog.show();
                            /*val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                                    val uri: Uri = Uri.fromParts("package", context!!.packageName, null)
                                    intent.data = uri
                                    startActivity(intent)*/
                            }else
                                checkAndRequestPermissions();

                        }
                    }
                }
            }
        }
    }
    /** Stops the camera. */
    @Override
    public void onPause() {
        super.onPause();
        if (callFaceProcess==0)
            return;
        preview.stop();
        isPaused = true;
        if (!isScanCompleted && !isScanCancelled) {
            callBackListeners.onError("Face scan Error - app is in background");
            if (getActivity()!=null)
                getActivity().finish();
        }
        if (startRecording>calibrationCounterTime && !isScanCompleted) {
            countTimer.cancel();
        }
        startRecording = 0;
        calibrationCounterTime = 0;
        calibrationCountTimer.cancel();
    }

    public void StopScan(boolean noCallback){
        if (!noCallback || startRecording < minTime && !isScanCompleted && !isScanCancelled){
            if (getActivity()!=null)
                getActivity().finish();
        }else {
            callBackListeners.onScanFinished(rgb,ppg_time, (processingCounter / avgFPSCounter),sendData);
        }
    }
    public void StopScan(){
        if (startRecording < minTime && !isScanCompleted && !isScanCancelled){
            if (getActivity()!=null)
                getActivity().finish();
        }else {
            callBackListeners.onScanFinished(rgb,ppg_time, (processingCounter / avgFPSCounter),sendData);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (cameraSource != null) {
            cameraSource.release();
        }
        if (startRecording>calibrationCounterTime && !isScanCompleted){
            countTimer.cancel();
            if (startRecording > minTime){
                callBackListeners.onScanFinished(rgb,ppg_time, (processingCounter / avgFPSCounter),sendData);
            }
        }

    }

    private final DecimalFormat df = new DecimalFormat("0.00");
    byte [] cameraData;
    int frameHeight,frameWidth;
    boolean isLightMode = false;

    @Override
    public void faceByte(byte[] data, Camera.Size previewSize, Camera camera) {
        frameHeight = previewSize.height;
        frameWidth = previewSize.width;
        cameraData = data;

        if (SystemClock.uptimeMillis() - mFPSTime > 1000) {
            mFPSTime = SystemClock.uptimeMillis();
            mFPS = mFPSCounter;
            mFPSCounter = 0;
        } else {
            mFPSCounter++;
        }

        if (callFaceProcess==0)
            return;
        if (startRecording>=calibrationCounterTime){
            if (face!=null){
                if (noFaceCounter!=null) {
                    scanWarning.setVisibility(View.GONE);
                    noFaceCounter.cancel();
                    noFaceCounter = null;
                }

                FaceLandmark RIGHT_CHEEK = face.getLandmark(FaceLandmark.RIGHT_CHEEK);
                FaceLandmark RIGHT_EYE = face.getLandmark(FaceLandmark.RIGHT_EYE);

                float RIGHTCHEEK_X = RIGHT_CHEEK.getPosition().x;
                float RIGHTCHEEK_Y = RIGHT_CHEEK.getPosition().y;
                float RIGHT_EYE_Y = RIGHT_EYE.getPosition().y;

                FaceLandmark LEFT_CHEEK = face.getLandmark(FaceLandmark.LEFT_CHEEK);

                float LEFTCHEEK_X = LEFT_CHEEK.getPosition().x;
                float LEFTCHEEK_Y = LEFT_CHEEK.getPosition().y;

                    /*Log.e("WH",frameWidth+","+frameHeight);
                    Log.e("Box",face.getBoundingBox().right+","+face.getBoundingBox().left +","
                            +face.getBoundingBox().top+","+face.getBoundingBox().bottom);*/

                if(face.getBoundingBox().right>frameHeight || face.getBoundingBox().left <0 ||
                        face.getBoundingBox().top <0 || face.getBoundingBox().bottom> frameWidth){
                    scanWarning.setText("You are too close to the screen.\nPlease move a bit far.");
                    scanWarning.setVisibility(View.VISIBLE);
                    startHandler1();
                    return;
                }
                if (closeFaceCounter!=null) {
                    scanWarning.setVisibility(View.GONE);
                    closeFaceCounter.cancel();
                    closeFaceCounter = null;
                }

                scanWarning.setVisibility(View.GONE);

                if(faceID==0){
                    faceID = faceId;
                }
                else if(faceID != faceId){
                    isScanCancelled = true;
                    callBackListeners.onError("Detect Multiple Faces");
                    if (getActivity()!=null)
                        getActivity().finish();
                    return;
                }

                processingCounter++;
                long new_time = System.currentTimeMillis();
                ppg_time.put(new_time - startTime1);

                float dx =(face.getBoundingBox().width()*1.0f)*0.075f;
                float dy = (RIGHTCHEEK_Y-RIGHT_EYE_Y)*0.5f;


                int xR1 = (int)(RIGHTCHEEK_X - dx);
                int yR1 = (int)(RIGHTCHEEK_Y - dy);

                int xR2 = (int)(RIGHTCHEEK_X + dx);
                int yR2 = (int)(RIGHTCHEEK_Y);

                int xL1 = (int)(LEFTCHEEK_X - dx);
                int yL1 = (int)(LEFTCHEEK_Y - dy);

                int xL2 = (int)(LEFTCHEEK_X + dx);
                int yL2 = (int)(LEFTCHEEK_Y);

                int count = 0;
                double sumR = 0;
                double sumG = 0;
                double sumB = 0;
                //Log.e("X,Y",""+xR1+","+yR1+","+xR2+","+yR2);

                // Gather pixel data for the square
                for (int i1 = yR1; i1 < yR2; i1++) {
                    for (int i2 = xR1; i2 < xR2; i2++) {
                        int color = getColorAtPoint(cameraData, i1, i2,frameHeight,frameWidth);
                        sumR += Color.red(color);
                        sumG += Color.green(color);
                        sumB += Color.blue(color);
                        count++;
                    }
                }

                // Gather pixel data for the square
                for (int i11 = yL1; i11 < yL2; i11++) {
                    for (int i21 = xL1; i21 < xL2; i21++) {
                        int color = getColorAtPoint(cameraData, i11, i21,frameHeight,frameWidth);
                        sumR += Color.red(color);
                        sumG += Color.green(color);
                        sumB += Color.blue(color);
                        count++;
                    }
                }

                // Average data
                sumR /= count;
                sumG /= count;
                sumB /= count;


                // Normalize data
                sumR = (sumR > 255) ? 255 : sumR;
                sumR = (sumR < 0) ? 0 : sumR;
                sumG = (sumG > 255) ? 255 : sumG;
                sumG = (sumG < 0) ? 0 : sumG;
                sumB = (sumB > 255) ? 255 : sumB;
                sumB = (sumB < 0) ? 0 : sumB;
                try {
                    raw_intensity = new JSONObject();

                    raw_intensity.put("r", df.format(sumR));
                    raw_intensity.put("g", df.format(sumG));
                    raw_intensity.put("b", df.format(sumB));

                    rgb.put(raw_intensity);
                    callBackListeners.OnFrame("scan",scanPercentage,df.format(mFPS), true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {
                faceID = 0;
                scanWarning.setVisibility(View.VISIBLE);
                scanWarning.setText("Cannot Detect Face");
                startHandler();
            }
        }
        /*if (isLightMode){
            if (startRecording>=calibrationCounterTime){
                if (face!=null){
                    if (noFaceCounter!=null) {
                        scanWarning.setVisibility(View.GONE);
                        noFaceCounter.cancel();
                        noFaceCounter = null;
                    }

                    FaceLandmark RIGHT_CHEEK = face.getLandmark(FaceLandmark.RIGHT_CHEEK);
                    FaceLandmark RIGHT_EYE = face.getLandmark(FaceLandmark.RIGHT_EYE);

                    float RIGHTCHEEK_X = RIGHT_CHEEK.getPosition().x;
                    float RIGHTCHEEK_Y = RIGHT_CHEEK.getPosition().y;
                    float RIGHT_EYE_Y = RIGHT_EYE.getPosition().y;

                    FaceLandmark LEFT_CHEEK = face.getLandmark(FaceLandmark.LEFT_CHEEK);

                    float LEFTCHEEK_X = LEFT_CHEEK.getPosition().x;
                    float LEFTCHEEK_Y = LEFT_CHEEK.getPosition().y;

                    */
        /*Log.e("WH",frameWidth+","+frameHeight);
                    Log.e("Box",face.getBoundingBox().right+","+face.getBoundingBox().left +","
                            +face.getBoundingBox().top+","+face.getBoundingBox().bottom);*/
        /*

                    if(face.getBoundingBox().right>frameHeight || face.getBoundingBox().left <0 ||
                            face.getBoundingBox().top <0 || face.getBoundingBox().bottom> frameWidth){
                        scanWarning.setText("You are too close to the screen.\nPlease move a bit far.");
                        scanWarning.setVisibility(View.VISIBLE);
                        startHandler1();
                        return;
                    }
                    if (closeFaceCounter!=null) {
                        scanWarning.setVisibility(View.GONE);
                        closeFaceCounter.cancel();
                        closeFaceCounter = null;
                    }

                    scanWarning.setVisibility(View.GONE);

                    if(faceID==0){
                        faceID = faceId;
                    }
                    else if(faceID != faceId){
                        isScanCancelled = true;
                        callBackListeners.onError("Detect Multiple Faces");
                        if (getActivity()!=null)
                    getActivity().finish();
                        return;
                    }

                    processingCounter++;
                    long new_time = System.currentTimeMillis();
                    ppg_time.put(new_time - startTime1);

                    float dx =(face.getBoundingBox().width()*1.0f)*0.075f;
                    float dy = (RIGHTCHEEK_Y-RIGHT_EYE_Y)*0.5f;


                    int xR1 = (int)(RIGHTCHEEK_X - dx);
                    int yR1 = (int)(RIGHTCHEEK_Y - dy);

                    int xR2 = (int)(RIGHTCHEEK_X + dx);
                    int yR2 = (int)(RIGHTCHEEK_Y);

                    int xL1 = (int)(LEFTCHEEK_X - dx);
                    int yL1 = (int)(LEFTCHEEK_Y - dy);

                    int xL2 = (int)(LEFTCHEEK_X + dx);
                    int yL2 = (int)(LEFTCHEEK_Y);

                    int count = 0;
                    double sumR = 0;
                    double sumG = 0;
                    double sumB = 0;
                    //Log.e("X,Y",""+xR1+","+yR1+","+xR2+","+yR2);

                    // Gather pixel data for the square
                    for (int i1 = yR1; i1 < yR2; i1++) {
                        for (int i2 = xR1; i2 < xR2; i2++) {
                            int color = getColorAtPoint(cameraData, i1, i2,frameHeight,frameWidth);
                            sumR += Color.red(color);
                            sumG += Color.green(color);
                            sumB += Color.blue(color);
                            count++;
                        }
                    }

                    // Gather pixel data for the square
                    for (int i11 = yL1; i11 < yL2; i11++) {
                        for (int i21 = xL1; i21 < xL2; i21++) {
                            int color = getColorAtPoint(cameraData, i11, i21,frameHeight,frameWidth);
                            sumR += Color.red(color);
                            sumG += Color.green(color);
                            sumB += Color.blue(color);
                            count++;
                        }
                    }

                    // Average data
                    sumR /= count;
                    sumG /= count;
                    sumB /= count;


                    // Normalize data
                    sumR = (sumR > 255) ? 255 : sumR;
                    sumR = (sumR < 0) ? 0 : sumR;
                    sumG = (sumG > 255) ? 255 : sumG;
                    sumG = (sumG < 0) ? 0 : sumG;
                    sumB = (sumB > 255) ? 255 : sumB;
                    sumB = (sumB < 0) ? 0 : sumB;

                    //Log.e("RGBR",""+(int)sumR+","+(int)sumG+","+(int)sumB);
    */
        /*Log.e("RGBR",""+j+","+k+","+m);
    Log.e("RGBL",""+j1+","+k1+","+m1);*/
        /*
                    try {
                        raw_intensity = new JSONObject();

                        raw_intensity.put("r", df.format(sumR));
                        raw_intensity.put("g", df.format(sumG));
                        raw_intensity.put("b", df.format(sumB));

                        rgb.put(raw_intensity);
                        callBackListeners.OnFrame("scan",scanPercentage,df.format(mFPS), true,isLightMode);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else {
                    faceID = 0;
                    scanWarning.setVisibility(View.VISIBLE);
                    scanWarning.setText("Cannot Detect Face");
                    startHandler();
                }
            }
            *//*if (startRecording == (maxTime+calibrationCounterTime)){
                stopProcessing();
            }*//*
        }*/
    }

    private int mFPS = 0;         // the value to show
    private int mFPSCounter = 0;  // the value to count
    private long mFPSTime = 0;
    int faceID = 0,faceId;
    int callFaceProcess = 0;
    Face face;
    @Override
    public void processFace(List<Face> faceList, int faceId) {
        Face face;
        callFaceProcess++;
        if (faceList.size()>0){
            if (noFaceCounter!=null) {
                scanWarning.setVisibility(View.GONE);
                noFaceCounter.cancel();
                noFaceCounter = null;
            }

            this.face = faceList.get(0);
            face = faceList.get(0);
            calibrationFPS++;
            this.faceId = faceId;

            /*if (isLightMode){
                Log.e("LightMode", ""+ true);
            }
            else {
                if (startRecording>=calibrationCounterTime){

                    if (noFaceCounter!=null) {
                        scanWarning.setVisibility(View.GONE);
                        noFaceCounter.cancel();
                        noFaceCounter = null;
                    }

                    FaceLandmark RIGHT_CHEEK = face.getLandmark(FaceLandmark.RIGHT_CHEEK);
                    FaceLandmark RIGHT_EYE = face.getLandmark(FaceLandmark.RIGHT_EYE);

                    float RIGHTCHEEK_X = RIGHT_CHEEK.getPosition().x;
                    float RIGHTCHEEK_Y = RIGHT_CHEEK.getPosition().y;
                    float RIGHT_EYE_Y = RIGHT_EYE.getPosition().y;

                    FaceLandmark LEFT_CHEEK = face.getLandmark(FaceLandmark.LEFT_CHEEK);

                    float LEFTCHEEK_X = LEFT_CHEEK.getPosition().x;
                    float LEFTCHEEK_Y = LEFT_CHEEK.getPosition().y;

                *//*Log.e("WH",frameWidth+","+frameHeight);
                Log.e("Box",face.getBoundingBox().right+","+face.getBoundingBox().left +","
                        +face.getBoundingBox().top+","+face.getBoundingBox().bottom);*//*

                    if(face.getBoundingBox().right>frameHeight || face.getBoundingBox().left <0 ||
                            face.getBoundingBox().top <0 || face.getBoundingBox().bottom> frameWidth){
                        scanWarning.setText("You are too close to the screen.\nPlease move a bit far.");
                        scanWarning.setVisibility(View.VISIBLE);
                        startHandler1();
                        return;
                    }
                    if (closeFaceCounter!=null) {
                        scanWarning.setVisibility(View.GONE);
                        closeFaceCounter.cancel();
                        closeFaceCounter = null;
                    }

                    scanWarning.setVisibility(View.GONE);
                    if(faceID==0){
                        faceID = faceId;
                    }
                    else if(faceID != faceId){
                        isScanCancelled = true;
                        callBackListeners.onError("Detect Multiple Faces");
                        if (getActivity()!=null)
                    getActivity().finish();
                        return;
                    }
                    processingCounter++;
                    long new_time = System.currentTimeMillis();
                    ppg_time.put(new_time - startTime1);

                    float dx =(face.getBoundingBox().width()*1.0f)*0.075f;
                    float dy = (RIGHTCHEEK_Y-RIGHT_EYE_Y)*0.5f;


                    int xR1 = (int)(RIGHTCHEEK_X - dx);
                    int yR1 = (int)(RIGHTCHEEK_Y - dy);

                    int xR2 = (int)(RIGHTCHEEK_X + dx);
                    int yR2 = (int)(RIGHTCHEEK_Y);

                    int xL1 = (int)(LEFTCHEEK_X - dx);
                    int yL1 = (int)(LEFTCHEEK_Y - dy);

                    int xL2 = (int)(LEFTCHEEK_X + dx);
                    int yL2 = (int)(LEFTCHEEK_Y);

                    int count = 0;
                    double sumR = 0;
                    double sumG = 0;
                    double sumB = 0;
                    //Log.e("X,Y",""+xR1+","+yR1+","+xR2+","+yR2);

                    // Gather pixel data for the square
                    for (int i1 = yR1; i1 < yR2; i1++) {
                        for (int i2 = xR1; i2 < xR2; i2++) {
                            int color = getColorAtPoint(cameraData, i1, i2,frameHeight,frameWidth);
                            sumR += Color.red(color);
                            sumG += Color.green(color);
                            sumB += Color.blue(color);
                            count++;
                        }
                    }

                    // Gather pixel data for the square
                    for (int i11 = yL1; i11 < yL2; i11++) {
                        for (int i21 = xL1; i21 < xL2; i21++) {
                            int color = getColorAtPoint(cameraData, i11, i21,frameHeight,frameWidth);
                            sumR += Color.red(color);
                            sumG += Color.green(color);
                            sumB += Color.blue(color);
                            count++;
                        }
                    }

                    // Average data
                    sumR /= count;
                    sumG /= count;
                    sumB /= count;


                    // Normalize data
                    sumR = (sumR > 255) ? 255 : sumR;
                    sumR = (sumR < 0) ? 0 : sumR;
                    sumG = (sumG > 255) ? 255 : sumG;
                    sumG = (sumG < 0) ? 0 : sumG;
                    sumB = (sumB > 255) ? 255 : sumB;
                    sumB = (sumB < 0) ? 0 : sumB;

                    //Log.e("RGBR",""+(int)sumR+","+(int)sumG+","+(int)sumB);
    *//*Log.e("RGBR",""+j+","+k+","+m);
    Log.e("RGBL",""+j1+","+k1+","+m1);*//*
                    try {
                        raw_intensity = new JSONObject();

                        raw_intensity.put("r", df.format(sumR));
                        raw_intensity.put("g", df.format(sumG));
                        raw_intensity.put("b", df.format(sumB));

                        rgb.put(raw_intensity);
                        callBackListeners.OnFrame("scan",scanPercentage,df.format(mFPS), true,isLightMode);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //Log.e("Area",""+((x2-x1)*(y2-y1))+"----"+i);
                }
                else {
                    if(face.getBoundingBox().right>frameHeight || face.getBoundingBox().left <0 ||
                            face.getBoundingBox().top <0 || face.getBoundingBox().bottom> frameWidth){
                        scanWarning.setText("You are too close to the screen.\nPlease move a bit far.");
                        scanWarning.setVisibility(View.VISIBLE);
                    }else{
                        scanWarning.setVisibility(View.GONE);
                    }
                }
                *//*if (startRecording == (maxTime+calibrationCounterTime)){
                    stopProcessing();
                }*//*
            }*/
        }
        else {
            this.face = null;
            if (startRecording>calibrationCounterTime){
                faceID = 0;
                scanWarning.setVisibility(View.VISIBLE);
                scanWarning.setText("Cannot Detect Face");
                startHandler();
            }
        }
        //Log.e("mFPS",""+mFPS);
    }

    private void stopProcessing() {
        try {
            sendData.put("raw_intensity", rgb);
            sendData.put("ppg_time", ppg_time);
            sendData.put("average_fps", processingCounter / avgFPSCounter);
            isScanCompleted = true;
            callBackListeners.onScanFinished(rgb,ppg_time, (processingCounter / avgFPSCounter),sendData);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void startHandler() {
        if (noFaceCounter!=null)
            return;

        noFaceCounter = new CountDownTimer(3 * 1000L, 1000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                isScanCancelled = true;
                callBackListeners.onError("Unable to measure your vitals.\nTry to look at the camera the next time");
                if (getActivity()!=null)
                    getActivity().finish();
            }
        }.start();
    }
    private void startHandler1() {
        if (closeFaceCounter!=null)
            return;

        closeFaceCounter = new CountDownTimer(3 * 1000L, 1000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                isScanCancelled = true;
                callBackListeners.onError("Unable to measure your vitals.\nTry to look at the camera the next time");
                if (getActivity()!=null)
                    getActivity().finish();
            }
        }.start();
    }

    public static int getColorAtPoint(byte[] yuv, int x, int y, int frameHeight, int frameWidth) {
        int i = (frameWidth * frameHeight) + frameWidth * (y >> 1) + (x & 0xFFFFFFFE);
        int j = 0xFF & yuv[x + y * frameWidth];
        int k = 0xFF & yuv[(i + 1)];
        int m = 0xFF & yuv[i];
        int n = k - 128;
        int i1 = m - 128;
        int i2 = (int) (j + 1.402f * i1);
        int i3 = (int) (j - 0.344f * n - 0.714f * i1);
        int i4 = (int) (j + 1.772f * n);
        i2 = (i2 < 0) ? 0 : i2;
        i2 = (i2 > 255) ? 255 : i2;
        i3 = (i3 < 0) ? 0 : i3;
        i3 = (i3 > 255) ? 255 : i3;
        i4 = (i4 < 0) ? 0 : i4;
        i4 = (i4 > 255) ? 255 : i4;
        return Color.rgb(i2, i3, i4);
    }
}

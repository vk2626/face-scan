package com.tech.facescan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button face_button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        face_button = findViewById(R.id.face_button);
        face_button.setOnClickListener(v->{
            startActivity(new Intent(this, StartFaceScan.class));
        });
    }
}